DISTRO ?= prime
MACHINE ?= intel-corei7-64
DEPLOY_SRCDIR ?= build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)
DESTDIR ?= /storage

IMAGES = \
	dom0-image \
	installer-netinst-image \
	ndvm-image

IMAGES_FS := $(foreach IMAGE,$(IMAGES),$(DEPLOY_SRCDIR)/$(IMAGE)-$(MACHINE).ext4)
XEN := build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)/xen-$(MACHINE).gz
VMLINUZ := build/tmp-$(DISTRO)-glibc/deploy/images/$(MACHINE)/bzImage-$(MACHINE).bin

.PHONY: all
all:
	./bb-wrapper --continue $(IMAGES)

.PHONY: dom0-image
dom0-image:
	./bb-wrapper dom0-image

.PHONY: installer-image
installer-image:
	./bb-wrapper installer-image

.PHONY: installer-net-image
installer-net-image:
	./bb-wrapper installer-net-image

.PHONY: ndvm-image
ndvm-image:
	./bb-wrapper ndvm-image

.PHONY: clean
clean:
	rm -rf build cache

.PHONY: install
install:
	mkdir -p $(DESTDIR)
	install -m 0755 $(IMAGES_FS) $(DESTDIR)/
	install -m 0755 $(XEN) $(DESTDIR)/
	install -m 0755 $(VMLINUZ) $(DESTDIR)/
